Repository for electronic susbsystems designed for TOTORO [BEXUS 33 cycle](https://rexusbexus.net/) project.  The aim of the project is to desing and built a system capable of registering [Auroral Kilmetric Radiation (RAS)](https://sci.esa.int/web/cluster/-/43028-auroral-kilometric-radiation).

<img src="totoro.png"  width="200" height="200">
<img src="bexus.jpg"  width="200" height="200">

During the project, as a Lead Electronics Engineer, I was responsible for:
- Radio Adjustment System (RAS),
- Active Antenna System,
- Data Acquisistion System (DAS),
- general electronics tasks, such as grounding scheme, SDR selection or interfaces planning.

The figure below presents the general overview of the Totoro electronics system.

<img src="experiment_overview.png"  width="700" height="500">

The system is made of two standalone stacks for each data acquisition channel. Each stack is made of a power supply unit (PSU), Raspberry Pi, DAS and RAS. The PSU also includes a heating system, while the temperature of the experiment is measured by DAS. The uplink and downlink is realised by a link provided by the BEXUS gondola. 

