function El = inductor_noise(fmin,fmax,L,T,SRF,Rp,R_series)
    
    % inductor_noise Calculates inductor noise for given bandwidth and
    % temperature
    %   L - inductance in H
    %   T - temperature in Kelvin
    %   fmin - min frequency
    %   fmax - max frequency
    %   SRF - self resonant frequency, has to ben taken from ds
    %   Rp - value of impednace at SRF
    %   R_series - ESR at DC
    
    k = physconst('Boltzmann');
    % equivalent parallel capactiance
    Cp = 1/(4*pi^2*L*SRF);
    % equivalnet series resistance
    if nargin ==5
        Rs = 1;
    else 
        Rs = R_series;
    end
    
    SPD =@(x) 4*T*k*(Rs -(((2*pi*x*L)^2*Rp)/(Rp^2*(1-L*Cp*(2*pi*x)^2+(2*pi*x)^2*L^2))));
    El = integral(SPD,fmin,fmax,'ArrayValued',true);
end