function Ec = cap_noise(C,T,fmin,fmax)
    
    % cap_noise Calculates capacitor noise for given bandwidth and
    % temperature
    %   C - capacitance in F
    %   T - temperature in Kelvin
    %   fmin - min frequency
    %   fmax - max frequency

    k = physconst('Boltzmann');
    %typical value of ESR for ceramic capacitors
    R1 = 0.1;
    %typical value of insulation resistance for ceramic capacitors
    R2 = 1e10;
    
    SPD =@(x) 4*T*k*(R1+R2/(1+2*pi*x*C^2*R2^2));
    Ec = integral(SPD,fmin,fmax,'ArrayValued',true);
end