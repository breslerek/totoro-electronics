Radio Adjustment System (RAS) is a set of RF filters and amplifiers for TOTORO project, intended for operation in 10 kHz - 1 MHz range. The RAS is resposnbile for signal conditioning (amplification and filtration) to restrict it to 10 kHz - 1 MHz badwidth and filter unwanted interferences. Figure below presents the block diagram of the system. 

<img src="RAS.png"  width="600" height="200">

Files include Matlab scripts to calculate RF path parameters and LTspice scripts to simulate its performance. Schamtics and PCB and done in Altium Designer 22. Figures below present the render of the designed amplifer and filter.  

<img src="ras_amp.PNG"  width="400" height="300">
<img src="ras_filter.PNG"  width="400" height="300">



