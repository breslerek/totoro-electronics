import matplotlib.pyplot as plt
import numpy as np
import ltspice
import os
import csv

def sim_ltspice(file = ''):

    l = ltspice.Ltspice(f'{file}.raw')    
    l.parse()
    vout = []
    freq = l.get_frequency()
    for i in range(l.case_count):
        vout.append(l.get_data('V(out)',i))
    return vout, freq
    
vout, freq = sim_ltspice('mc_init_tol')
freq = np.array(freq)/1000000
fig, axs = plt.subplots(3,2)
colors = ['tab:blue','tab:orange','tab:green','tab:red','tab:purple','tab:pink']
a = 0
b = 0
for i in range(0,6):
    data = np.array(vout[i])
    mag = 20*np.log10(np.absolute(data))
    axs[a, b].plot(freq, mag, colors[i])
    plt.grid()
    res = np.where(mag < -3)[0]
    axs[a, b].set_title(f'MC run {i+1} {freq[res[0]]*1000:.1f}-{freq[res[-1]]*1000:.1f} kHz')
    axs[a,b].grid()
    a = a + 1
    if a == 3:
        a = 0
        b = 1
for ax in axs.flat:
    ax.set(ylabel='Magnitude [dB]', xlabel='Frequency [MHz]')
    
for ax in axs.flat:
    ax.label_outer()


plt.show()
