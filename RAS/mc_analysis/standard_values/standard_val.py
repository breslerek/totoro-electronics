import matplotlib.pyplot as plt
import numpy as np
import ltspice
import os
import csv

def sim_ltspice(file = ''):

    l = ltspice.Ltspice(f'{file}.raw')    
    l.parse()
    freq = l.get_frequency()
    vout = l.get_data('V(out)')
    return vout, freq
    
vout, freq = sim_ltspice('standard_values')
data = np.array(vout)
freq = np.array(freq)/1000000
mag = 20*np.log10(np.absolute(data))
res = np.where(mag < -3)[0]
j = 0
while freq[res[j]]*1000<466:
    j += 1
    
    
print(freq[res[-1]])
plt.plot(freq,mag)
plt.title(f'Standard values {freq[res[j]]*1000:.1f}-{freq[res[-1]]*1000:.1f} kHz')
plt.xlabel("Frequency [MHz]")
plt.ylabel("Amplitude [dB]")
plt.grid()

