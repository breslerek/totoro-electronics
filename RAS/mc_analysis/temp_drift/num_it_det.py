import matplotlib.pyplot as plt
import numpy as np
import ltspice
import os
import csv

def sim_ltspice(file = ''):

    l = ltspice.Ltspice(f'{file}.raw')    
    l.parse()
    vout = []
    freq = l.get_frequency()
    for i in range(l.case_count):
        vout.append(l.get_data('V(out)',i))
    return vout, freq
    
vout, freq = sim_ltspice('mc_temp_val')
freq = np.array(freq)/1000
index1 = np.where(freq > 462)[0]
index1 = index1[0]
index2 = np.where(freq > 470)[0]
index2 = index2[0]
center_freq = []
for i in range(0,len(vout)):
    data = np.array(vout[i]) 
    mag = 20*np.log10(np.absolute(data))   
    index3 = np.argmax(mag[index1:index2])
    index4 = np.where(mag < -3)[0]
    j = 0
    while index4[j] < index1+index3:
        j += 1
        
    start = index4[j]
    stop = index4[-1]
    center_freq.append((freq[start]+freq[stop])/2)   

center_freq = np.array(center_freq)
x_mean = np.mean(center_freq) 
x_dev =  np.std (center_freq)
z = 1.96
accuracy = 0.1
n_it = np.power(z*x_dev/accuracy,2)
print(x_mean, x_dev, n_it) 
with open('number_of_iterations.txt', 'w') as f:
    f.write(f'required number of iterations for accuracy of {accuracy} is {n_it}') 

