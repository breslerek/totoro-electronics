clear all; close all; 

rootname = 'amp';
out=LTspice2Matlab([rootname,'.raw']);
freq = out.freq_vect;
sig = out.variable_mat;
angle = unwrap(rad2deg(angle(sig)));
amp = 20*log10(abs(sig));
figure(1)
yyaxis left
semilogx(freq/1000,amp,"LineWidth", 2);
ylabel('Amplitude [dB]')
yyaxis right
semilogx(freq/1000,angle,"LineWidth", 2);
grid on
xlabel('Frequency [kHz]')
ylabel('Phase [deg]')
%%

figure(2)
x = linspace(0,10);
y = sin(3*x);
yyaxis left
semilogx(x,y)

z = sin(3*x).*exp(0.5*x);
yyaxis right
semilogx(x,z)
ylim([-150 150])
 
