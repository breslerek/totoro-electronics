function F = fcel(x)
    global B; 
    global data freq;
    %% filter response calculation
    L_cel = zeros(1,length(x)/2);
    C_cel = zeros(1,length(x)/2);
    for i = 1:length(x)/2
        L_cel(i) = x(i);
        C_cel(i) = x(i+length(x)/2);
    end
    rf_filter_cel = rfckt.lcbandstoptee('C', C_cel,'L',L_cel);
    analyze(rf_filter_cel,freq,50,50);
    data = calculate(rf_filter_cel,'S11','S21','dB');
    S21 = cell2mat(data(2));
    %% f goal calc
    lim1 = B(1);
    lim2 = B(2);
    [data_set, data_domain] = get_data(lim1,lim2,freq,S21);
    F = smooth_data(data_set, data_domain);
end