function data = smooth_data(data_set, data_domain)
[f_max, x_max] = max(data_set);
if x_max == 1 || x_max == numel(data_domain)
    data = f_max;
else 
    [coeffs,~,mu] = polyfit(data_domain(x_max-1:x_max+1)-data_domain(x_max), ...
        data_set(x_max-1:x_max+1) ,2);
    maks=-coeffs(2)/(2*coeffs(1));
    data = polyval(coeffs,maks,[],mu);
end

end