%% RF_filter designer
%
% Description: Matlab script which designs a RF bandstop filter
% for given bandwidth and attentuation
%
% Requirements:
%   RF Toolbox
%   Global Optimization Toolbox
% Inputs:
%   B           - stopbandwidth of filter in Hz
%   fmin        - min frequency for analysis in Hz
%   fmax        - max frequency for analysis in Hz
%   PassBanAtt  - max attenuation in passband
%   StopBanAtt  - min attenuation in stopband
%   forder      - filter order
%
% Revision: R2020b 
% Author: Karol Bresler
% Date: 08.01.2020
%---------------------------------------------------------
close all; clear all;
global E24 E12 multiplier;
E24 = [1.0, 1.1, 1.2, 1.3, 1.5, 1.6, 1.8, 2.0, 2.2, 2.4, 2.7, 3.0, 3.3, ...
            3.6, 3.9, 4.3, 4.7, 5.1, 5.6, 6.2, 6.8, 7.5, 8.2, 9.1, 10]; 
E12 = [1.0, 1.2, 1.5, 1.8, 2.2, 2.7, 3.3, 3.9, 4.7, 5.6, 6.8, 8.2,  10]; 

%% Specify filter parameters
global B freq;
fmin = 1e3; fmax = 1e7;
B = [485e3 515e3];
freq = fmin:1000:fmax;
PassBanAtt = 1; StopBanAtt = 30; forder = 5;

%% Design prototype filter with non-series element values
[L_values, C_values, S11, S21] = RF_prototype_design(PassBanAtt,...
    StopBanAtt, forder);
figure(1)
semilogx(freq/1000,S21, freq/1000, S11);
xlabel('Frequency [kHz]')
ylabel('S21 [dB]')
grid on

%% Adjust to E24 series values
[L_values_int, C_values_int, S11, S21, x, exponent] = RF_int_design...
    (L_values, C_values);
figure(2)
semilogx(freq/1000,S21, freq/1000, S11);
xlabel('Frequency [kHz]')
ylabel('S21 [dB]')
grid on
L_filter = L_values_int;
C_filter = C_values_int;

%% Optimize filter
val = input("Should the integer optimization be performed?(1/0):");
if val == 1
    [L_values_opt, C_values_opt, S11,S21] = RF_optimize(x);
    figure(3)
    semilogx(freq/1000,S21, freq/1000, S11);
    grid on
    xlabel('Frequency [kHz]')
    ylabel(' S21 [dB]')
    L_filter = L_values_opt;
    C_filter = C_values_opt;
end

%% Save data
no = 1:forder;
L_text = 'Inductors values';
C_text = 'Capacitors values';
L_data = [no',x(1:forder)',exponent(1:forder)'];
C_data = [no',x(forder+1:end)',exponent(forder+1:end)'];
L_ltspice = [];
C_ltspice = [];
for i=1:forder
    expr = ['Lw' num2str(i) '=' num2str(x(i)*multiplier(i)*1e6) 'u '];
    L_ltspice = [L_ltspice, expr]; 
    expr = ['Cw' num2str(i) '=' num2str(x(i+forder)*multiplier(i+forder)*1e9) 'n '];
    C_ltspice = [C_ltspice, expr]; 
end
s = what('RF_passive_notch_filter');
path = regexprep(s.path,'(\w+)$','');
path = fullfile(path, 'RF_path_params');
filename=['RF_notch_' num2str(B(1)/1000) '_' num2str(B(2)/1000) '_kHz_'...
    num2str(forder) '_order.csv'];
file = fullfile(path, filename);
writematrix(L_text,file);
writematrix(L_data,file,'WriteMode','append');
writematrix(L_ltspice,file,'WriteMode','append');
writematrix(C_text,file,'WriteMode','append');
writematrix(C_data,file,'WriteMode','append');
writematrix(C_ltspice,file,'WriteMode','append');

 