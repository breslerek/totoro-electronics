function [L_values, C_values, S11, S21] = RF_prototype_design(PassBanAtt, ...
       StopBanAtt, forder)
%% RF_prototype_design
%
% Description: Designs bandstop RF filter with non-series element values. a
%
% Inputs:
%   B           - stopbandwidth of filter in Hz
%   PassBanAtt  - max attenuation in passband
%   StopBanAtt  - min attenuation in stopband
%   forder      - filter order
%
% Outputs:
%   L_values - inductors' values
%   C_values - capacitors' values'
%   S11      - S11 filter parameter
%   S21      - S21 filter parameter
%
% Revision: R2020b 
% Author: Karol Bresler
% Date: 08.01.2020
%---------------------------------------------------------    
    global B freq;
    % calculate parameters values
    filter = rffilter("FilterType","Chebyshev","ResponseType","Bandstop", ...
        "PassbandAttenuation",PassBanAtt,"StopbandFrequency",B, ...
        "FilterOrder",forder, "StopbandAttenuation",StopBanAtt, ...
        "Implementation", "LC Tee");
    L_values = zeros(1,forder);
    C_values = zeros(1,forder);
    for i = 1:forder
        L_values(i) = filter.DesignData.Inductors(i);
        C_values(i) = filter.DesignData.Capacitors(i);
    end
    % analyze filter's response
    rf_filter = rfckt.lcbandstoptee('C', C_values,'L', L_values);
    analyze(rf_filter,freq,50,50);
    data = calculate(rf_filter,'S11','S21','dB');
    S11 = cell2mat(data(1));
    S21 = cell2mat(data(2));
end