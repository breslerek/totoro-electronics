function [y_data, x_data] = get_data(value1, value2, x_values, y_values)
    [~, index1] = min(abs(x_values-value1));
    [~, index2] = min(abs(x_values-value2));
    y_data = y_values(index1:index2);
    x_data = x_values(index1:index2);
end
