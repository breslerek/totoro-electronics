function [L_values_opt, C_values_opt, S11,S21] = RF_optimize(x)
%% RF_optimize
%
% Description: Integer optimization of given RF filter.
%
% Inputs:
%   x - filter elements' values in single vector (inductors first, then
%   capacitors)
%
% Outputs:
%   L_values_opt - optimized inductors' values
%   C_values_opt - optimized capacitors' values'
%   S11      - S11 filter parameter
%   S21      - S21 filter parameter
%
% Revision: R2020b 
% Author: Karol Bresler
% Date: 08.01.2020
%---------------------------------------------------------
%needs rework for E12 inductors
    global freq E24 E12;
    global multiplier;
    x0 = x;
    n = length(x0);
    lb = ones(1,length(x0)); 
    ub = 10*ones(1,length(x0)); 
    [phi_lb, phi_ub] = get_const(lb,ub,E24);
    phi_x0 = zeros(length(x0),1);
    for i=1:n/2
        phi_x0(i) = find(E12==x0(i));
        phi_x0(i+n/2) = find(E24==x0(i+n/2)); 
    end 
    opts = optimoptions('surrogateopt', 'Display', 'iter', 'InitialPoints', ...
        phi_x0, 'MaxFunctionEvaluations', 500);
    intcon = 1:length(x0);
    diary 'surrogateopt.txt'
    diary on
    tic
    [x,fval,exitflag,output,trials] = surrogateopt(@objconstr, ...
        phi_lb,phi_ub,intcon,opts);
    toc
    diary off
    L_values_opt = zeros(1,n/2);
    C_values_opt = zeros(1,n/2);
    phi_E24=@(z)spline([1:numel(E24)],E24, z);
    phi_E12=@(z)spline([1:numel(E12)],E12, z);
    phi_x = [phi_E12(x(1:n/2)), phi_E24(x(n/2+1:end))];
    %phi=@(z)spline([1:numel(E24)],E24, z);
    values_opt = phi_x.*multiplier;
    for i = 1:n/2
        L_values_opt(i) = values_opt(i);
        C_values_opt(i) = values_opt(i+n/2);
    end
    rf_filter_opt = rfckt.lcbandstoptee('C', C_values_opt,'L', L_values_opt);
    analyze(rf_filter_opt,freq,50,50);
    data = calculate(rf_filter_opt,'S11','S21','dB');
    S11 = cell2mat(data(1));
    S21 = cell2mat(data(2));

end