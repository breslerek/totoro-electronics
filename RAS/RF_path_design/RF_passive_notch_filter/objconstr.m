function f = objconstr(x)

    global multiplier E24 E12;
    n = length(x);
    phi_E24=@(z)spline([1:numel(E24)],E24, z);
    phi_E12=@(z)spline([1:numel(E12)],E12, z);
    phi_x = [phi_E12(x(1:n/2)), phi_E24(x(n/2+1:end))];
    f.Fval = fcel(phi_x.*multiplier);
    [f.Ineq ~] = f_nonlcon(phi_x.*multiplier);

end