function [phi_lb, phi_ub] = get_const(lb, ub , E24)

for i=1:length(lb) 
    for j = 1:length(E24)
        if(E24(j) >= lb(i))
            phi_lb(i) = j;
            break
        end
    end
    
    for j = length(E24):-1:1
        if(E24(j) <= ub(i))
            phi_ub(i) = j;
            break
        end
    end
end
