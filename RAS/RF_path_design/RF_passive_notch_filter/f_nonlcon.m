function [c,ceq] = f_nonlcon(x)
    global B data freq; 
    %% filter response calculation
    S11 = cell2mat(data(1));
    %% out of band values to B(1)
    lim1 = 1e3;
    lim2 = B(1)-1e4;
    [data_set1, data_domain1] = get_data(lim1,lim2,freq,S11);
    data_set1 = (data_set1 + 5)/10;  
    %% in band values
    lim1 = B(1)+5e3;
    lim2 = B(2)-5e3;
    [data_set2, data_domain2] = get_data(lim1,lim2,freq,S11);
    data_set2 = (-1*data_set2-1)/2;
    smoothed_data = smooth_data(data_set2, data_domain2);
    %% out of band values from B(2)
    lim1 = B(2)+1e4;
    lim2 = 1e6;
    [data_set3, data_domain3] = get_data(lim1,lim2,freq,S11);
    data_set3 = (data_set3 + 5)/10;    
    %% final values
    data_set = [max(data_set1);smoothed_data;max(data_set3)];
    c = max(data_set)
    ceq = [];
end
