function [L_values_int, C_values_int, S11, S21, x, exponent] = ...
    RF_int_design(L_values, C_values)
 %% RF_int_design
%
% Description: Designs bandstop RF filter with E24 series element values. 
%
% Inputs:
%   L_values    - non-series values of inductors 
%   C_values    - non-series values of capacitors
%
% Outputs:
%   L_values_int - inductors' values
%   C_values_int - capacitors' values'
%   S11          - S11 filter parameter
%   S21          - S21 filter parameter
%   x            - elements' values in single vector
%
% Revision: R2020b 
% Author: Karol Bresler
% Date: 08.01.2020
%---------------------------------------------------------     
    global freq E24 E12;
    global multiplier;
    L = L_values;
    C = C_values;
    x = [L_values C_values];  
    n = length(x);
    multiplier = zeros(1,n);
    exponent = zeros(1,n);
    m = 0;
    % Integer values assignment for inductors 
    for i = 1:n/2
        while(L(i) < 1)
            L(i) = L(i)*10;
            m = m+1;
        end 
        [~, index] = min(abs(E12-L(i)));
        L(i) = E12(index);
        multiplier(i) = 10^(-1*m);
        exponent(i) = -1*m;
        m = 0;
    end 
    j = i+1;
    % Integer values assignment for capacitors 
    for i = 1:n/2
        while(C(i) < 1)
            C(i) = C(i)*10;
            m = m+1;
        end 
        [~, index] = min(abs(E24-C(i)));
        C(i) = E24(index);
        multiplier(j) = 10^(-1*m);
        exponent(j) = -1*m;
        j = j+1;
        m = 0;
    end
    
    L_values_int = zeros(1,n/2);
    C_values_int = zeros(1,n/2);
    x = [L C];
    for i = 1:n/2
        L_values_int(i) = L(i)*multiplier(i);
        C_values_int(i) = C(i)*multiplier(i+n/2);
    end
    % analyze filter's response
    rf_filter_int = rfckt.lcbandstoptee('C', C_values_int,'L', L_values_int);
    analyze(rf_filter_int,freq,50,50);
    data = calculate(rf_filter_int,'S11','S21','dB');
    S11 = cell2mat(data(1));
    S21 = cell2mat(data(2));
end