clear all; close all; 

rootname = 'rf_path';
out=LTspice2Matlab([rootname,'.raw']);
freq = out.freq_vect;
sig = out.variable_mat;
angle = unwrap(rad2deg(angle(sig)));
amp = 20*log10(abs(sig));
figure(1)
yyaxis left
semilogx(freq/1000,amp,"LineWidth", 2);
ylabel('Amplitude [dB]')
yyaxis right
semilogx(freq/1000,angle,"LineWidth", 2);
grid on
xlabel('Frequency [kHz]')
ylabel('Phase [deg]')

 
