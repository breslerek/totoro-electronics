function [time,ch_lt] = LTspice_data(rootname) 
time=[]; 
ch_lt=[];
[status,result]=system(['run_sim.bat ', rootname]); 
if status~=0
    error(sprintf('run_sim.bat call failure, status=%d, result=%s\n',result));
end
out=LTspice2Matlab([rootname,'.raw']);
time=out.time_vect(:); 
ch_lt(1,:)=out.variable_mat(2,:);
ch_lt(2,:)=out.variable_mat(6,:);
ch_lt(3,:)=out.variable_mat(3,:);
ch_lt(4,:)=out.variable_mat(5,:);
ch_lt(5,:)=out.variable_mat(4,:);
ch_lt(6,:)=out.variable_mat(1,:);
end