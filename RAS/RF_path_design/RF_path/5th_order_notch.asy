Version 4
SymbolType BLOCK
RECTANGLE Normal 64 32 -64 -32
TEXT -3 -47 Center 2 5th_order_filter
SYMATTR Prefix X
SYMATTR Value 5th_order_notch
PIN -64 -16 LEFT 8
PINATTR PinName 1
PINATTR SpiceOrder 1
PIN -64 16 LEFT 8
PINATTR PinName 2
PINATTR SpiceOrder 2
PIN 64 -16 RIGHT 8
PINATTR PinName 3
PINATTR SpiceOrder 3
PIN 64 16 RIGHT 8
PINATTR PinName 4
PINATTR SpiceOrder 4
