import matplotlib.pyplot as plt
import numpy as np
import ltspice
import os
import csv

def sim_ltspice(file = ''):

    l = ltspice.Ltspice(f'results/{file}.raw')    
    l.parse()
    vout = []
    freq = l.get_frequency()
    for i in range(l.case_count):
        vout.append(l.get_data('V(out)',i))
    return vout, freq

vout, freq = sim_ltspice('miniwhip')
freq = np.array(freq/1000)
data = np.array(vout)
mag = 20*np.log10(np.absolute(data))
phase = np.rad2deg(np.unwrap(np.angle(data)))+180

#data visualisation for spectrum results
fig, ax1 = plt.subplots()
color = 'tab:blue'
ax1.set_xlabel('f (kHz)')
ax1.set_xscale('log')
ax1.set_ylabel('Magnitude (dB)', color=color)
ax1.plot(freq ,mag[6], color=color, label = 'Input impedance')
ax2 = ax1.twinx()  
color = 'tab:red'
ax2.set_ylabel('Theta (o)', color=color)  
ax2.plot(freq,phase[6], linestyle='dashdot', color=color)
ax2.tick_params(axis='y', labelcolor=color)
fig.tight_layout()

plt.figure()
for i in range(0,len(vout)):
    plt.plot(freq,mag[i], label=f'Temp={-50+i*10}')
    
plt.xscale('log')
plt.xlabel('f (kHz)')
plt.ylabel('Magnitude (dB)')
#plt.legend(loc="lower left")

plt.show()