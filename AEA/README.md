Active electric antenna subsystem is absed on the miniwhip antenna idea. The design allows to register signals form 10 kHz to 1 MHz range while using realitvely short antenna, when compared to registered wavelengths. The figure below presents the magnitude and phase of the deisgned antenna over 1 kHz to 100 MHz badwidth. 

<img src="aea_bandwidth.png"  width="400" height="300">

Figures below present the render of the designed antenna and bias required for antennas proper operation. 

<img src="miniwhip.PNG"  width="400" height="300">
<img src="bias.PNG"  width="400" height="300">
